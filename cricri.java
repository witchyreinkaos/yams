import java.util.Arrays; 
import java.util.Scanner;
public class App {

 public static void display (int [] input)
 {  System.out.println("affichage du tableau");
     for (int i=0;i!=input.length;i++)
     {
         System.out.println(input[i]);
     }
 }
 // méthode renvoyant un tableau des cases non encore remplies

 public static void displaycheck (boolean [] check)
 {  System.out.println("affichage des choix possibles");
     for (int i=0;i!=check.length;i++)
     {   
         if (check[i]==false)
         {System.out.println("vous pouvez choisir la case: "+i);}
     }
 }
 public static int [] lancer (int [] init)
 {
    int [] randy= new int [init.length];
    for (int k=0;k!=5;k++)
    {randy[k]=(int)(Math.random()*6+ 1);
        
    }
    return randy;
 }
 // méthode qui renvoie un jet de nbddés
 public static int [] lancern (int [] init,int nbdedés)
 {
    int [] randy= new int [nbdedés];
    for (int k=0;k!=nbdedés;k++)
    {randy[k]=(int)(Math.random()*6+ 1);
        
    }
    return randy;
 }


 public static int [] occurences (int [] results)
 {   int [] score=new int [results.length+1];
    for (int j=0;j!=6;j++)
        {
        for (int i=0;i!=5;i++)
    {
        if (results[i]==j+1)
        score[j]=score[j]+1;
    }}
    

    return score;



 }
// méthode renvoyant la valeur du dé d'un brelan (3 en argument) ou d'un carré (4)
public static int get(int [] occur, int nnn)

{   int value=0;
    for (int k=0;k!=6;k++)
    {if (occur[k]==nnn)
    value=k+1;}
    return value;
}

 public static boolean brelan (int [] occur,boolean [] checkytab1)
 {  boolean inter=false;
    boolean brelan1=false;
    for (int i=0;i!=6;i++)
    {if (occur[i]==3)

        {
        System.out.println("le joueur 1 a un brelan pour la valeur: "+(i+1));
        // attention mauvais score!!!
        System.out.println("Cela correspond à un score de: "+(i+1)*3);
        brelan1=true;
        }
    }
    if ((brelan1==true)&&(checkytab1[6]==false))
    {   
        System.out.println("vous pouvez choisir ce brelan");
        inter=true;}
    return inter;
     
 }
 // détection d'une grande suite
 public static boolean grandesuite (int [] occur,boolean [] checkytab1)
 {
   
int[] grandesuite1 = new int[] {0,1, 1, 1, 1,1};
int[] grandesuite2 = new int[] {1,1, 1, 1, 1,0};
boolean grand=false;

if (Arrays.equals(grandesuite1,occur)||Arrays.equals(grandesuite2,occur))
{System.out.println("Grande suite pour le joueur !!!!");
System.out.println("Cela correspond à un score de 50 points");
if (checkytab1[10]==false) 
{grand=true;}
}

    return grand;

    

 }
 // méthode de détection d'un carré renvoit vrai si carré et carré non déjà rempli
 public static boolean carre (int[] occur,boolean [] checkytab1)
 {
    int valeurcarré=0;
    boolean square1=false;
    for (int i=0;i!=6;i++)
    {if (occur[i]==4)

        {valeurcarré=(i+1);
        System.out.println("le joueur 1 a un carré pour la valeur: "+(i+1));
        // attention calcul de score non valide!!!
        System.out.println("Cela correspond à un score de: "+(i+1)*4);
        
        if (checkytab1[7]==false) // vérifie si déjà placé
        {square1=true;
            System.out.println("Vous pouvez placer ce carré");

        }


        }
        
    }
    return square1;
 }

 // méthode pour la détection de yams
 public static boolean yams (int[] occur,boolean [] checkytab1)
    {
        int c0=0;
        int c5=0;
        boolean yami1=false;
        for (int i=0;i!=6;i++) // comptage des zéros et des 5
        {if (occur[i]==0)
        {
            c0=c0+1;
    
        }
        if (occur[i]==5)
        {c5=c5+1;}
    
        }   
        // test de yams ( cinq zéros et un 5 dans le tableau d'occurences)
        if ((c0==5)&&(c5==1))
        {System.out.println("Vous avez un Yams!!!!");
        if (checkytab1[11]==false)
        {System.out.println("Vous pouvez placer ce Yams!!!!");
        yami1=true;} 

    
    }
    return yami1;
    }
    public static boolean fully (int [] occur,boolean [] checkytab1)
    {
        // test de full, un brelan et une paire
    
    boolean b31=false;
    boolean b21=false;
    boolean fully1=false;
    for (int i=0;i!=6;i++)
    {
        if (occur[i]==3)
        {b31=true;
        }

    }
    for (int i=0;i!=6;i++)
    {if (occur[i]==2)
    {b21=true;

    }}
    if ((b21==true)&&(b31==true))
    {
        
        
        if (checkytab1[8]==false)
        {   System.out.println("Full!!!!");
            System.out.println("vous pouvez placer ce full!!!");
            fully1=true;}
        
    }
    return fully1;
    }
    public static void sc16 (int [] occur,boolean [] checkytab1)
    {
    for (int i=0;i!=6;i++)
    {   
        
        if (checkytab1[i]==false)
        {   System.out.println("score possible pour la valeur: "+(i+1));
            System.out.println(occur[i]*(i+1));
            }

    }
    

    }
    //méthode renvoyant true si petite suite 
    public static boolean petite (int [] occur, boolean [] checkytab1)
    {   boolean naine=false;
        // énumération des 14 petites suites
        int [] p1 ={1,1,1,1,0,1};
        int [] p2 ={2,1,1,1,0,0};
        int [] p3 ={1,2,1,1,0,0};
        int [] p4 ={1,1,2,1,0,0};
        int [] p5 ={1,1,1,2,0,0};
        int [] p6 ={0,2,1,1,1,0};
        int [] p7 ={0,1,2,1,1,0};
        int [] p8 ={0,1,1,2,1,0};
        int [] p9 ={0,1,1,1,2,0};
        int [] p10={1,0,1,1,1,1};
        int [] p11={0,0,2,1,1,1};
        int [] p12={0,0,1,2,1,1};
        int [] p13={0,0,1,1,2,1};
        int [] p14={0,0,1,1,1,2};
        boolean intx1=(Arrays.equals(p1,occur))||(Arrays.equals(p2,occur))||(Arrays.equals(p3,occur));
        boolean intx2=(Arrays.equals(p4,occur))||(Arrays.equals(p5,occur))||(Arrays.equals(p6,occur));
        boolean intx3=(Arrays.equals(p7,occur))||(Arrays.equals(p8,occur))||(Arrays.equals(p9,occur));
        boolean intx4=(Arrays.equals(p10,occur))||(Arrays.equals(p11,occur))||(Arrays.equals(p12,occur));
        boolean intx5=(Arrays.equals(p13,occur))||(Arrays.equals(p14,occur));
        if ((intx1==true)||(intx2==true)||(intx3==true)||(intx4==true)||(intx5==true))
        {   if (checkytab1[9]==false)
            {naine=true;
        System.out.println("vous avez une petite suite et vous pouvez la placer");}

        
            }
            return naine;
        

    } 
        // méthode qui renvoie les choix possibles en fonction des occurences et des cases déjà remplies
        // non finie!!! et non utilisée dans le main
    public static String choixutilisateurs (boolean [] checkytab1,int [] occur)
    { // déclaration scanner
            // scanner à déclarer
            for(int k=0;k!=6;k++)
            {
                if (checkytab1[k]==false)
                {
                    System.out.println("choix possible:  "+(k+1));
                }
            } 
            if (brelan(occur, checkytab1)==true)
                    {System.out.println("entrer b pour brelan");}

           // entrée clavier   
           return "réponse";   
    }
    // méthode renvoyant le total des dés pour la case chance
    public static int chance (int [] results,boolean [] checkytab1)
    { int total5=0;
        if (checkytab1[12]==false)
        {
        for (int k=0;k!=5;k++)
        total5=total5+results[k];}
        return total5;
    }

    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        // une manche

        int [] initial={0,0,0,0,0};
        boolean [] checkytab1=new boolean[13]; // false si case pas encore cochée
        boolean [] endofgame=new boolean[13]; // endofgame est un tableau de 13 true
        int [] points = new int [13]; // tableau des points initialisés à 0
        for (int k=0;k!=13;k++)
        {endofgame[k]=true;}
        // créer un point des totaux
        boolean manchefinie=false;
        // boucle tant que manchefinie égale false
       
        int [] test=lancer(initial); // jet de dés stocké dans test
        display(test); // affichage du jet de dés
        display(occurences(test)); // affichage des occurences de 1 à 6
        //sc16(occurences(test),checkytab1);
        
        
       // premiers tests des méthodes 
        /*int [] fullboy={0,5,0,0,0,0};
        int [] carreboy={4,1,0,0,0,0};
        int [] sweetie={0,2,1,1,1,0};
        boolean yu;
        boolean car;
        boolean yoman;
        yoman=petite(sweetie, checkytab1);
        yu=yams(fullboy,checkytab1);
        car=carre(carreboy,checkytab1);
        System.out.println("test lancer de taille 4");
        int [] test2=lancern(initial,4);
        display(test2);
        // tests de détection d'un brelan

        // test pour fin de manche*/
        
        sc16(occurences(test), checkytab1);
        brelan(occurences(test),checkytab1);
        carre(occurences(test), checkytab1);
        fully(occurences(test), checkytab1);
        petite(occurences(test), checkytab1);
        grandesuite(occurences(test), checkytab1);
        yams(occurences(test), checkytab1);
        System.out.println("score chance: ");
        System.out.println(chance(test, checkytab1));
        
        displaycheck(checkytab1);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez votre choix");
        int choix = scanner.nextInt();
        checkytab1[choix]=true;
        // calcul des scores et remplissage de la table des scores
        // à tester!!!
        if (choix<=5)
        {points[choix]=(choix+1)*(occurences(test)[choix]);
        checkytab1[choix]=true;}
        // brelan
        if (choix==6)
        {points[choix]=3*get(occurences(test),3);
            checkytab1[choix]=true;}
        if (choix==7)
        {points[choix]=4*get(occurences(test),4);
            checkytab1[choix]=true;}
        if (choix==8)
        { checkytab1[8]=true;
        points[8]=25;}
        if (choix==9)
        {checkytab1[9]=true;
        points[9]=30;}
        if (choix==10)
        {checkytab1[10]=true;
        points[10]=40;}
        if (choix==11)
        {checkytab1[11]=true;
        points[11]=50;}
        if (choix==12)
        {points[12]=chance(occurences(test), checkytab1);}
            
                   
    }
    }